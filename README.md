---
Template: Example plain HTML website using GitLab with [Netlify](https://www.netlify.com/).
Sourced: https://scrimba.com/scrim/cVgVNNSE

---

## Netlify Configuration

In order to build this site with Netlify, simply log in or register at 
https://app.netlify.com/, then select "New site from Git" from the top
right. Select GitLab, authenticate if needed, and then select this
project from the list. 

You will need to set the publish directory to `/public`. Netlify will handle the 
rest.
